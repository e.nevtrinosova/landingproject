const postcssPresetEnv = require('postcss-preset-env');

module.exports = {
	plugins: [
		require('postcss-import'),
		require('postcss-custom-media'),
		require('postcss-media-minmax'),
		require('postcss-extend-rule'),
		postcssPresetEnv({
			stage: 0,
		}),
		require('cssnano')
	],
};