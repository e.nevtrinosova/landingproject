const path = require("path");

const HtmlWebPackPlugin = require("html-webpack-plugin");
const htmlWebpackPlugin = new HtmlWebPackPlugin({
	template: "./src/index.html",
	filename: "./index.html"
});

const CleanWebpackPlugin = require("clean-webpack-plugin");
const cleanWebpackPlugin = new CleanWebpackPlugin(["dist"]);

module.exports = {
	entry: {
		index: "./src/index.js",
	},
	output: {
		path: path.resolve("dist"),
		filename: "[name].js",
		publicPath: '/'
	},
	module: {
		rules: [
			{
				test: /\.js?$/,
				exclude: /node_modules/,
				use: {
					loader: "babel-loader"
				}
			},
			{
				test: /\.css$/,
				exclude: /node_modules/,
				use: [
					"style-loader",
					{
						loader: 'css-loader',
						options: {
							importLoaders: 1,
						}
					},
				]
			}
		]
	},
	resolve: {
		modules: [
			'node_modules',
		],
	},
	plugins: [htmlWebpackPlugin, cleanWebpackPlugin],
	devServer: {
		historyApiFallback: true
	}
};