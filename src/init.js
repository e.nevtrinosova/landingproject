export default grapesjs.init({
	container: '#gjs',
	fromElement: true,

	height: '900px',
	width: 'auto',

	storageManager: {
		id: 'gjs-landing-',
		type: 'local',
		autosave: true,
		autoload: true,
		stepsBeforeSave: 1,
	},

	panels: {
		defaults: [{
			id: 'layers',
			el: '.panel__right',
		}]
	},

	styleManager: {
		appendTo: '.styles-container',
		sectors: [{
			name: 'Styles',
			open: true,
			buildProps: ["background", "background-color", "color"],
			properties: [
				{
					property : 'background',
					name: 'Background Image',
				},
			]
		}
		]
	},

	domComponents: {
		stylePrefix: 'comp-',

		wrapperId: 'wrapper',

		wrapperName: 'Body',

		wrapper: {
			removable: false,
			copyable: false,
			draggable: false,
			components: [],
			traits: [],
			stylable: [
				'background',
				'background-image',
				'color',
			]
		},
	},

});
