import SwitchPanel from "../src/components/SwitchPanel";
import React from "react";
import ReactDOM from "react-dom";
import editor from './init';

import "./styles.css";

ReactDOM.render(<SwitchPanel />, document.getElementById('root'));

editor.Panels.addPanel({
	id: 'panel-top',
	el: '.panel__top',
});
editor.Panels.addPanel({
	id: 'basic-actions',
	el: '.panel__basic-actions',
	buttons: [
		// {
		// 	id: 'visibility',
		// 	active: true, // active by default
		// 	className: 'btn-toggle-borders',
		// 	label: '<u>B</u>',
		// 	command: 'sw-visibility', // Built-in command
		// },
		{
			id: 'export',
			className: 'btn-open-export',
			label: 'Код страницы',
			command: 'export-template',
			context: 'export-template', // For grouping context of buttons from the same panel
		}
	],
});
