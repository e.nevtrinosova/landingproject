import editor from "../../init";

const iconText = () => {
	const domComponents = editor.DomComponents;

	domComponents.addComponent(`<link rel="stylesheet" type="text/css" href="build/css/main.css">`);

	domComponents.addComponent(
		`<div class="icon-grid__header">
			<div class="icon-grid__block">
				<div class="icon-grid__icon"><img src="/src/assets/icons/B_1.svg"></div>
				<div class="icon-grid__title-container"><p class="icon-grid__title">Невероятно стильный заголовок</p></div>
				<p class="icon-grid__main-text">Тебя ждут лекции, мастер-классы, разбор твоей работы и ответы на волнующие вопросы от ведущих специалистов. Свежие идеи, заряд вдохновения и поддержка едино- мышленников это лишь малая часть того, что с тобой может — произойти после интенсива.</p>
				<a class="icon-grid__more-info">Узнать подробнее</a><img src="/src/assets/icons/icon_navigation.svg">
			</div>
			<div class="icon-grid__block">
				<div class="icon-grid__icon"><img src="/src/assets/icons/B_2.svg"></div>
				<div class="icon-grid__title-container"><p class="icon-grid__title">Второй невероятно стильный и обворожительный заголовок</p></div>
				<p class="icon-grid__main-text">На первый взгляд, может показаться, что это значительно облегчает прохождение. На самом деле, начальные локации <span class="icon-grid__main-text__name">дают возможность</span> запастись деньгами, предметами и улучшениями для здоровья, которые сильно облегчат жизнь на поздних этапах.</p>
				<button class="icon-grid__button">Войти в почту</button>
			</div>
			<div class="icon-grid__block">
				<div class="icon-grid__icon"><img src="/src/assets/icons/B_3.svg"></div>
				<div class="icon-grid__title-container"><p class="icon-grid__title">Третий невероятно стильный заголовок</p></div>
				<p class="icon-grid__main-text">Игроки предполагали, что Sony откажется от цены в 3999 рублей ещё в начале 2017 года из-за «налога на Google», однако тогда в компании сообщили, что постараются удерживать эту стоимость как можно дольше. При этом некоторые партнёры Sony всё же повысили цены — например, игры Warner Bros. тогда подорожали до 4399 рублей.</p>
			</div>
		</div>`);
};

export default iconText;