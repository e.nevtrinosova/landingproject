import editor from "../../init";

const iconText = () => {
	const domComponents = editor.DomComponents;

	domComponents.addComponent(`<link rel="stylesheet" type="text/css" href="build/css/main.css">`);

	domComponents.addComponent(
		`<div class="icon-text-list__header">
			<div class="icon-text-list__block">
				<div class="icon-text-list__icon"><img src="/src/assets/icons/B_1.svg"></div>
				<div class="icon-text-list__content">
					<div class="icon-text-list__title-container"><p class="icon-text-list__title">Невероятно стильный заголовок</p></div>
					<p class="icon-text-list__main-text">Тебя ждут лекции, мастер-классы, разбор твоей работы и ответы на волнующие вопросы от ведущих специалистов. Свежие идеи, заряд вдохновения и поддержка едино- мышленников это лишь малая часть того, что с тобой может — произойти после интенсива.</p>
					<a class="icon-text-list__more-info">Узнать подробнее</a><img src="/src/assets/icons/icon_navigation.svg">
				</div>
			</div>
			<div class="icon-text-list__block">
				<div class="icon-text-list__icon"><img src="/src/assets/icons/B_2.svg"></div>
				<div class="icon-text-list__content">
					<div class="icon-text-list__title-container"><p class="icon-text-list__title">Второй невероятно стильный и обворожительный заголовок</p></div>
					<p class="icon-text-list__main-text">На первый взгляд, может показаться, что это значительно облегчает прохождение. На самом деле, начальные локации <span class="icon-text-list__main-text__name">дают возможность</span> запастись деньгами, предметами и улучшениями для здоровья, которые сильно облегчат жизнь на поздних этапах.</p>
					<button class="icon-text-list__button">Войти в почту</button>
				</div>
			</div>
			<div class="icon-text-list__block">
				<div class="icon-text-list__icon"><img src="/src/assets/icons/B_3.svg"></div>
				<div class="icon-text-list__content">
					<div class="icon-text-list__title-container"><p class="icon-text-list__title">Третий невероятно стильный заголовок</p></div>
					<p class="icon-text-list__main-text">Игроки предполагали, что Sony откажется от цены в 3999 рублей ещё в начале 2017 года из-за «<a class="icon-text-list__link">налога на Google</a>», однако тогда в компании сообщили, что постараются удерживать эту стоимость как можно дольше. При этом некоторые партнёры Sony всё же повысили цены — например, игры Warner Bros. тогда подорожали до 4399 рублей.</p>
				</div>
			</div>
			<div class="icon-text-list__block">
				<div class="icon-text-list__icon"><img src="/src/assets/icons/B_4.svg"></div>
				<div class="icon-text-list__content">
					<div class="icon-text-list__title-container"><p class="icon-text-list__title">Четвёртый царский и православный, невероятно стильный и сочный заголовок</p></div>
					<p class="icon-text-list__main-text">Также есть улучшения, которые в некоторой степени облегчают игру, но не совсем. Например, в Spelunky есть возможность <span class="icon-text-list__main-text__name">открывать короткие пути</span>. Если заплатить одному NPC, то он предоставит доступ к тоннелю, который может привести пользователя в определённую <span class="icon-text-list__main-text__name">локацию</span>. Благодаря этому, герой может пропустить первые четыре уровня в шахтах и перенестись сразу в джунгли.</p>
				</div>
			</div>
		</div>`);
};

export default iconText;