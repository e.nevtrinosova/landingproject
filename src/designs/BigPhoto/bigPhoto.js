import editor from "../../init";

const bigPhoto = () => {
	const domComponents = editor.DomComponents;

	domComponents.addComponent(`<link rel="stylesheet" type="text/css" href="build/css/main.css">`);

	domComponents.addComponent(`
			<div class="big-photo">
				<div class="big-photo__title-container"><p class="big-photo__title">Невероятно стильный заголовок</p></div>
				<div class="big-photo__content-container">
					<img src="https://bestcube.space/wp-content/uploads/0Jrvgf38V8.jpg">
				</div>
			</div>`);
};

export default bigPhoto;