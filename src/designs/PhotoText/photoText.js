import editor from "../../init";

const photoText = () => {
	const domComponents = editor.DomComponents;

	domComponents.addComponent(`<link rel="stylesheet" type="text/css" href="build/css/main.css">`);

	domComponents.addComponent(`
			<div class="photo-text">
				<div class="photo-text__element">
					<div class="photo-text__element__content-part">
						<p class="photo-text__element__title">Невероятно стильный заголовок</p>
						<p class="photo-text__element__main-info"><span class="photo-text__element__name">Design Line Intensive</span> — это неделя прокачки твоих дизайн-навыков. Тебя ждут лекции, мастер-классы</p>
						<p class="photo-text__element__secondary-info">Тебя ждут лекции, мастер-классы, разбор твоей работы и ответы на волнующие вопросы от ведущих специалистов. Свежие идеи, заряд вдохновения и поддержка единомышленников это лишь малая часть того, что с тобой может — произойти после интенсива.</p>
					</div>
					<div class="photo-text__element__photo-part">
						<div class="photo-text__element__photo-container"><img src="http://www.radionetplus.ru/uploads/posts/2013-04/1365401196_teplye-oboi-1.jpeg"></div>
						<div class="photo-text__element__comment-left"><p class="photo-text__element__comment">Тебя ждут лекции, мастер-классы, разбор твоей работы и ответы на волнующие вопросы от ведущих специалистов.</p></div>
					</div>
				</div>
				<div class="photo-text__element-reverse">
					<div class="photo-text__element__content-part">
						<p class="photo-text__element__title">Второй невероятно стильный и сочный заголовок</p>
						<p class="photo-text__element__secondary-info">Тебя ждут лекции, мастер-классы, разбор твоей работы и ответы на волнующие вопросы от ведущих специалистов. Свежие идеи, заряд вдохновения и поддержка единомышленников это лишь малая часть того, что с тобой может — произойти после интенсива.</p>
						<a class="photo-text-more-info">Узнать подробнее</a><img src="/src/assets/icons/icon_navigation.svg">
					</div>
					<div class="photo-text__element__photo-part">
						<div class="photo-text__element__photo-container"><img src="http://www.radionetplus.ru/uploads/posts/2013-04/1365401196_teplye-oboi-1.jpeg"></div>
						<div class="photo-text__element__comment-right"><p class="photo-text__element__comment">Тебя ждут лекции, мастер-классы, разбор твоей работы и ответы на волнующие вопросы от ведущих специалистов.</p></div>
					</div>
				</div>
				<div class="photo-text__element">
					<div class="photo-text__element__content-part">
						<p class="photo-text__element__title">Третий невероятно стильный заголовок</p>						
						<p class="photo-text__element__secondary-info">Игроки предполагали, что Sony откажется от цены в 3999 рублей ещё в начале 2017 года из-за «налога на Google», однако тогда в компании сообщили, что постараются удерживать эту стоимость как можно дольше. При этом некоторые партнёры Sony всё же повысили цены — например, игры Warner Bros. тогда подорожали до 4399 рублей.</p>
						<a class="photo-text-more-info">Узнать подробнее</a><img src="/src/assets/icons/icon_navigation.svg">
					</div>
					<div class="photo-text__element__photo-part">
						<div class="photo-text__element__photo-container"><img src="http://www.radionetplus.ru/uploads/posts/2013-04/1365401196_teplye-oboi-1.jpeg"></div>
						<div class="photo-text__element__comment-left"><p class="photo-text__element__comment">Тебя ждут лекции, мастер-классы, разбор твоей работы и ответы на волнующие вопросы от ведущих специалистов.</p></div>
					</div>
				</div>
			</div>`);
};

export default photoText;
