import editor from "../../init";

const header = () => {
	const domComponents = editor.DomComponents;

	domComponents.addComponent(`<link rel="stylesheet" type="text/css" href="build/css/main.css">`);

	const main = domComponents.addComponent(`<div class="header__main"></div>`);
	const mainComponents = main.get('components');

	mainComponents.add({
		tagName: 'div',
		content: `<img src="/src/assets/icons/mail_logo.svg"/>`,
		removable: false,
		style: {
			'display': 'flex',
			'align-items': 'center',
			'flex-direction': 'column'
		}
	});

	mainComponents.add(`
			<div class="header__title-container"><p class="header__title">Невероятно стильный заголовок</p></div>
			<div class="header__content-container">
				<p class="header__main-text">
					<span class="header__main-text__name">Design Line Intensive</span> — это неделя прокачки твоих дизайн-навыков.
						Тебя ждут лекции, мастер-классы, разбор твоей работы и ответы на волнующие вопросы от ведущих специалистов. 
						Свежие идеи, заряд вдохновения и поддержка единомышленников - это лишь малая часть того, что с тобой может
			 			произойти после интенсива.
				</p>
				<button class="header__button">Войти в почту</button>
				<div class="header__comment">Тебя ждут лекции, мастер-классы, разбор твоей работы</div>
			</div>`);
};

export default header;
