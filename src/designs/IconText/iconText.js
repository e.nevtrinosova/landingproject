import editor from "../../init";

const iconText = () => {
	const domComponents = editor.DomComponents;

	domComponents.addComponent(`<link rel="stylesheet" type="text/css" href="build/css/main.css">`);

	const main = domComponents.addComponent(`<div class="icon-text__header"></div>`);
	const mainComponents = main.get('components');

	const content = mainComponents.add(`<div></div>`);
	const contentComponent = content.get('components');

	contentComponent.add({
		tagName: 'div',
		content: `<img src="/src/assets/icons/mail_logo.svg"/>`,
		removable: false,
		style: {
			'display': 'flex',
			'justify-content': 'left',
		}
	});

	contentComponent.add(
		`<div class="icon-text__title-container"><p class="icon-text__title">Невероятно стильный заголовок</p></div>
		<div class="icon-text__content-container">
			<p class="icon-text__main-text">
				Тебя ждут <span class="icon-text__main-text__name">лекции, мастер-классы, разбор твоей работы</span> и ответы на волнующие вопросы от ведущих специалистов.
				Свежие идеи, заряд вдохновения и поддержка единомышленников это лишь малая часть того, что с тобой может — произойти после интенсива.
			</p>
			<button class="icon-text__button">Войти в почту</button>
			<div class="icon-text__comment">Тебя ждут лекции, мастер-классы, разбор твоей работы</div>
		</div>`);

	mainComponents.add(`<div class="icon-text__icon"><img src="/src/assets/icons/B_4.svg"/></div>`);
};

export default iconText;