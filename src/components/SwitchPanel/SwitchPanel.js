import React from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from "@material-ui/core/Typography/Typography";
import { withStyles } from '@material-ui/core/styles';
import header from '../../designs/Header/header';
import iconText from '../../designs/IconText/iconText';
import iconGrid from '../../designs/IconGrid/iconGrid';
import iconTextList from '../../designs/IconTextList/iconTextList';
import bigPhoto from '../../designs/BigPhoto/bigPhoto';
import photoText from '../../designs/PhotoText/photoText';

import './SwitchPanel.css';

const styles = {
	root: {
		width: '100%',
		maxWidth: 360,
	},
	text: {
		color: '#FFFFFF',
	}
};

class SwitchPanel extends React.Component {
	render() {
		return <div className="switch-panel">
			<List component="nav">
				<ListItem button onClick={header}>
					<ListItemText
						primary={<Typography style={styles.text}>Header</Typography>}/>
					</ListItem>
				<ListItem button onClick={iconText}>
					<ListItemText
						primary={<Typography style={styles.text}>IconText</Typography>}/>
				</ListItem>
				<ListItem button onClick={iconGrid}>
					<ListItemText
						primary={<Typography style={styles.text}>IconGrid</Typography>}/>
				</ListItem>
				<ListItem button onClick={iconTextList}>
					<ListItemText
						primary={<Typography style={styles.text}>IconTextList</Typography>}/>
				</ListItem>
				<ListItem button onClick={bigPhoto}>
					<ListItemText
						primary={<Typography style={styles.text}>BigPhoto</Typography>}/>
				</ListItem>
				<ListItem button onClick={photoText}>
					<ListItemText
						primary={<Typography style={styles.text}>PhotoText</Typography>}/>
				</ListItem>
			</List>
		</div>;
	}
}

export default withStyles(styles)(SwitchPanel);
